EESchema Schematic File Version 4
LIBS:clinostat_esp32-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Personal_Library:XL4015_BOARD U2
U 1 1 5F99E8ED
P 1300 1000
F 0 "U2" H 1300 1225 50  0000 C CNN
F 1 "XL4015_BOARD" H 1300 1134 50  0000 C CNN
F 2 "Personal_Footprint_Library:XL4015_BOARD" H 1300 1000 50  0001 C CNN
F 3 "" H 1300 1000 50  0001 C CNN
	1    1300 1000
	1    0    0    -1  
$EndComp
$Comp
L power:+24V #PWR0103
U 1 1 5F99FA37
P 850 900
F 0 "#PWR0103" H 850 750 50  0001 C CNN
F 1 "+24V" H 865 1073 50  0000 C CNN
F 2 "" H 850 900 50  0001 C CNN
F 3 "" H 850 900 50  0001 C CNN
	1    850  900 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 5F99FFA6
P 850 1200
F 0 "#PWR0104" H 850 950 50  0001 C CNN
F 1 "GND" H 855 1027 50  0000 C CNN
F 2 "" H 850 1200 50  0001 C CNN
F 3 "" H 850 1200 50  0001 C CNN
	1    850  1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	850  900  850  1000
Wire Wire Line
	850  1000 950  1000
Wire Wire Line
	850  1200 850  1100
Wire Wire Line
	850  1100 950  1100
$Comp
L power:+5V #PWR0105
U 1 1 5F9A096E
P 1750 900
F 0 "#PWR0105" H 1750 750 50  0001 C CNN
F 1 "+5V" H 1765 1073 50  0000 C CNN
F 2 "" H 1750 900 50  0001 C CNN
F 3 "" H 1750 900 50  0001 C CNN
	1    1750 900 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0106
U 1 1 5F9A1470
P 1750 1200
F 0 "#PWR0106" H 1750 950 50  0001 C CNN
F 1 "GND" H 1755 1027 50  0000 C CNN
F 2 "" H 1750 1200 50  0001 C CNN
F 3 "" H 1750 1200 50  0001 C CNN
	1    1750 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	1650 1000 1750 1000
Wire Wire Line
	1750 1000 1750 900 
Wire Wire Line
	1650 1100 1750 1100
Wire Wire Line
	1750 1100 1750 1200
$Comp
L dk_PMIC-Voltage-Regulators-Linear:LD1117V33 U3
U 1 1 5F9A28D3
P 4250 1050
F 0 "U3" H 4250 1337 60  0000 C CNN
F 1 "LD1117V33" H 4250 1231 60  0000 C CNN
F 2 "digikey-footprints:TO-220-3" H 4450 1250 60  0001 L CNN
F 3 "http://www.st.com/content/ccc/resource/technical/document/datasheet/99/3b/7d/91/91/51/4b/be/CD00000544.pdf/files/CD00000544.pdf/jcr:content/translations/en.CD00000544.pdf" H 4450 1350 60  0001 L CNN
F 4 "497-1491-5-ND" H 4450 1450 60  0001 L CNN "Digi-Key_PN"
F 5 "LD1117V33" H 4450 1550 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 4450 1650 60  0001 L CNN "Category"
F 7 "PMIC - Voltage Regulators - Linear" H 4450 1750 60  0001 L CNN "Family"
F 8 "http://www.st.com/content/ccc/resource/technical/document/datasheet/99/3b/7d/91/91/51/4b/be/CD00000544.pdf/files/CD00000544.pdf/jcr:content/translations/en.CD00000544.pdf" H 4450 1850 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/stmicroelectronics/LD1117V33/497-1491-5-ND/586012" H 4450 1950 60  0001 L CNN "DK_Detail_Page"
F 10 "IC REG LINEAR 3.3V 800MA TO220AB" H 4450 2050 60  0001 L CNN "Description"
F 11 "STMicroelectronics" H 4450 2150 60  0001 L CNN "Manufacturer"
F 12 "Active" H 4450 2250 60  0001 L CNN "Status"
	1    4250 1050
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0107
U 1 1 5F9A3E98
P 2700 900
F 0 "#PWR0107" H 2700 750 50  0001 C CNN
F 1 "+5V" H 2715 1073 50  0000 C CNN
F 2 "" H 2700 900 50  0001 C CNN
F 3 "" H 2700 900 50  0001 C CNN
	1    2700 900 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0108
U 1 1 5F9A48A2
P 4250 1500
F 0 "#PWR0108" H 4250 1250 50  0001 C CNN
F 1 "GND" H 4255 1327 50  0000 C CNN
F 2 "" H 4250 1500 50  0001 C CNN
F 3 "" H 4250 1500 50  0001 C CNN
	1    4250 1500
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0109
U 1 1 5F9A50E7
P 4600 950
F 0 "#PWR0109" H 4600 800 50  0001 C CNN
F 1 "+3.3V" H 4615 1123 50  0000 C CNN
F 2 "" H 4600 950 50  0001 C CNN
F 3 "" H 4600 950 50  0001 C CNN
	1    4600 950 
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0110
U 1 1 5F9A6BB6
P 1900 3300
F 0 "#PWR0110" H 1900 3150 50  0001 C CNN
F 1 "+5V" H 1915 3473 50  0000 C CNN
F 2 "" H 1900 3300 50  0001 C CNN
F 3 "" H 1900 3300 50  0001 C CNN
	1    1900 3300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0111
U 1 1 5F9A78CA
P 1900 6100
F 0 "#PWR0111" H 1900 5850 50  0001 C CNN
F 1 "GND" H 1905 5927 50  0000 C CNN
F 2 "" H 1900 6100 50  0001 C CNN
F 3 "" H 1900 6100 50  0001 C CNN
	1    1900 6100
	1    0    0    -1  
$EndComp
$Comp
L RF_Module:ESP32-WROOM-32D U1
U 1 1 5F9C381E
P 1900 4700
F 0 "U1" H 2500 6250 50  0000 C CNN
F 1 "ESP32-WROOM-32D" H 2750 6100 50  0000 C CNN
F 2 "Personal_Footprint_Library:AZ-Delivery_ESP32" H 1900 3200 50  0001 C CNN
F 3 "https://www.espressif.com/sites/default/files/documentation/esp32-wroom-32d_esp32-wroom-32u_datasheet_en.pdf" H 1600 4750 50  0001 C CNN
	1    1900 4700
	1    0    0    -1  
$EndComp
NoConn ~ 1300 3500
NoConn ~ 1300 3700
NoConn ~ 1300 3800
Text Notes 2750 3800 0    50   ~ 0
Consider decoupling capacitors\nnear the power pin of the chip
$Comp
L Connector_Generic:Conn_01x02 J2
U 1 1 5F9F2723
P 5650 1100
F 0 "J2" H 5730 1092 50  0000 L CNN
F 1 "5V_RAIL" H 5730 1001 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 5650 1100 50  0001 C CNN
F 3 "~" H 5650 1100 50  0001 C CNN
	1    5650 1100
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0101
U 1 1 5F9F3616
P 5350 1050
F 0 "#PWR0101" H 5350 900 50  0001 C CNN
F 1 "+5V" H 5365 1223 50  0000 C CNN
F 2 "" H 5350 1050 50  0001 C CNN
F 3 "" H 5350 1050 50  0001 C CNN
	1    5350 1050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0113
U 1 1 5F9F3C92
P 5350 1300
F 0 "#PWR0113" H 5350 1050 50  0001 C CNN
F 1 "GND" H 5355 1127 50  0000 C CNN
F 2 "" H 5350 1300 50  0001 C CNN
F 3 "" H 5350 1300 50  0001 C CNN
	1    5350 1300
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J4
U 1 1 5F9F4F28
P 6650 1100
F 0 "J4" H 6730 1092 50  0000 L CNN
F 1 "3V3_RAIL" H 6730 1001 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 6650 1100 50  0001 C CNN
F 3 "~" H 6650 1100 50  0001 C CNN
	1    6650 1100
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0114
U 1 1 5F9F553E
P 6350 1050
F 0 "#PWR0114" H 6350 900 50  0001 C CNN
F 1 "+3.3V" H 6365 1223 50  0000 C CNN
F 2 "" H 6350 1050 50  0001 C CNN
F 3 "" H 6350 1050 50  0001 C CNN
	1    6350 1050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0115
U 1 1 5F9F5F35
P 6350 1300
F 0 "#PWR0115" H 6350 1050 50  0001 C CNN
F 1 "GND" H 6355 1127 50  0000 C CNN
F 2 "" H 6350 1300 50  0001 C CNN
F 3 "" H 6350 1300 50  0001 C CNN
	1    6350 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	6450 1100 6350 1100
Wire Wire Line
	6350 1100 6350 1050
Wire Wire Line
	5350 1050 5350 1100
Wire Wire Line
	5350 1100 5450 1100
Wire Wire Line
	5450 1200 5350 1200
Wire Wire Line
	5350 1200 5350 1300
Wire Wire Line
	6350 1300 6350 1200
Wire Wire Line
	6350 1200 6450 1200
Text GLabel 2500 4600 2    50   Input ~ 0
DIR
Text GLabel 2500 4500 2    50   Input ~ 0
STEP
Text GLabel 2500 4300 2    50   Input ~ 0
SCL
Text GLabel 2500 4100 2    50   Input ~ 0
SDA
Text GLabel 2500 5700 2    50   Input ~ 0
SPEED_POT
Text GLabel 2500 5800 2    50   Input ~ 0
SPI_RESET
Text GLabel 2500 5500 2    50   Input ~ 0
SPI_DC
Text GLabel 2500 5100 2    50   Input ~ 0
SPI_MOSI
Text GLabel 2500 4700 2    50   Input ~ 0
SPI_SCK
Text GLabel 2500 4000 2    50   Input ~ 0
SPI_SS
$Comp
L Connector_Generic:Conn_01x04 J3
U 1 1 5FA01373
P 5650 2100
F 0 "J3" H 5730 2092 50  0000 L CNN
F 1 "ACCEL_I2C" H 5730 2001 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 5650 2100 50  0001 C CNN
F 3 "~" H 5650 2100 50  0001 C CNN
	1    5650 2100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0119
U 1 1 5FA0308C
P 5200 2450
F 0 "#PWR0119" H 5200 2200 50  0001 C CNN
F 1 "GND" H 5205 2277 50  0000 C CNN
F 2 "" H 5200 2450 50  0001 C CNN
F 3 "" H 5200 2450 50  0001 C CNN
	1    5200 2450
	1    0    0    -1  
$EndComp
Text GLabel 5450 2200 0    50   Input ~ 0
SDA
Text GLabel 5450 2300 0    50   Input ~ 0
SCL
Wire Wire Line
	5200 2450 5200 2100
Wire Wire Line
	5200 2100 5450 2100
Wire Wire Line
	5450 2000 5350 2000
Wire Wire Line
	5350 2000 5350 1900
$Comp
L Connector_Generic:Conn_01x02 J5
U 1 1 5FA072AB
P 6650 1900
F 0 "J5" H 6730 1892 50  0000 L CNN
F 1 "MOTOR_CTL" H 6730 1801 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 6650 1900 50  0001 C CNN
F 3 "~" H 6650 1900 50  0001 C CNN
	1    6650 1900
	1    0    0    -1  
$EndComp
Text GLabel 6450 1900 0    50   Input ~ 0
DIR
Text GLabel 6450 2000 0    50   Input ~ 0
STEP
Text Notes 5650 1800 0    50   ~ 0
I2C
Text Notes 6850 1800 0    50   ~ 0
Motor
$Comp
L Connector_Generic:Conn_01x08 J6
U 1 1 5FA09003
P 8650 1400
F 0 "J6" H 8730 1392 50  0000 L CNN
F 1 "SCREEN_SPI" H 8730 1301 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x08_P2.54mm_Vertical" H 8650 1400 50  0001 C CNN
F 3 "~" H 8650 1400 50  0001 C CNN
	1    8650 1400
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0120
U 1 1 5FA09BDD
P 8350 1000
F 0 "#PWR0120" H 8350 850 50  0001 C CNN
F 1 "+3.3V" H 8365 1173 50  0000 C CNN
F 2 "" H 8350 1000 50  0001 C CNN
F 3 "" H 8350 1000 50  0001 C CNN
	1    8350 1000
	1    0    0    -1  
$EndComp
Text GLabel 8450 1200 0    50   Input ~ 0
SPI_RESET
Text GLabel 8450 1300 0    50   Input ~ 0
SPI_DC
Text GLabel 8450 1400 0    50   Input ~ 0
SPI_SS
Text GLabel 8450 1500 0    50   Input ~ 0
SPI_MOSI
Text GLabel 8450 1600 0    50   Input ~ 0
SPI_SCK
$Comp
L power:GND #PWR0121
U 1 1 5FA0BE1C
P 8350 1850
F 0 "#PWR0121" H 8350 1600 50  0001 C CNN
F 1 "GND" H 8355 1677 50  0000 C CNN
F 2 "" H 8350 1850 50  0001 C CNN
F 3 "" H 8350 1850 50  0001 C CNN
	1    8350 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	8450 1800 8350 1800
Wire Wire Line
	8350 1800 8350 1850
Wire Wire Line
	8350 1000 8350 1100
Wire Wire Line
	8350 1100 8450 1100
Wire Wire Line
	8450 1700 8000 1700
Wire Wire Line
	8000 1700 8000 1100
Wire Wire Line
	8000 1100 8350 1100
Connection ~ 8350 1100
$Comp
L Device:Ferrite_Bead_Small FB1
U 1 1 5FA11348
P 3600 1050
F 0 "FB1" V 3363 1050 50  0000 C CNN
F 1 "100 @ 100MHz" V 3454 1050 50  0000 C CNN
F 2 "Ferrite_THT:LairdTech_28C0236-0JW-10" V 3530 1050 50  0001 C CNN
F 3 "~" H 3600 1050 50  0001 C CNN
	1    3600 1050
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H1
U 1 1 5FA12293
P 10300 4350
F 0 "H1" V 10254 4500 50  0000 L CNN
F 1 "MountingHole_Pad" V 10345 4500 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 10300 4350 50  0001 C CNN
F 3 "~" H 10300 4350 50  0001 C CNN
	1    10300 4350
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H2
U 1 1 5FA12FBF
P 10300 4550
F 0 "H2" V 10254 4700 50  0000 L CNN
F 1 "MountingHole_Pad" V 10345 4700 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 10300 4550 50  0001 C CNN
F 3 "~" H 10300 4550 50  0001 C CNN
	1    10300 4550
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H3
U 1 1 5FA132B6
P 10300 4750
F 0 "H3" V 10254 4900 50  0000 L CNN
F 1 "MountingHole_Pad" V 10345 4900 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 10300 4750 50  0001 C CNN
F 3 "~" H 10300 4750 50  0001 C CNN
	1    10300 4750
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H4
U 1 1 5FA135C7
P 10300 4950
F 0 "H4" V 10254 5100 50  0000 L CNN
F 1 "MountingHole_Pad" V 10345 5100 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 10300 4950 50  0001 C CNN
F 3 "~" H 10300 4950 50  0001 C CNN
	1    10300 4950
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0122
U 1 1 5FA13B8A
P 10100 5100
F 0 "#PWR0122" H 10100 4850 50  0001 C CNN
F 1 "GND" H 10105 4927 50  0000 C CNN
F 2 "" H 10100 5100 50  0001 C CNN
F 3 "" H 10100 5100 50  0001 C CNN
	1    10100 5100
	1    0    0    -1  
$EndComp
Wire Wire Line
	10100 5100 10100 4950
Wire Wire Line
	10100 4350 10200 4350
Wire Wire Line
	10200 4550 10100 4550
Connection ~ 10100 4550
Wire Wire Line
	10100 4550 10100 4350
Wire Wire Line
	10200 4750 10100 4750
Connection ~ 10100 4750
Wire Wire Line
	10100 4750 10100 4550
Wire Wire Line
	10200 4950 10100 4950
Connection ~ 10100 4950
Wire Wire Line
	10100 4950 10100 4750
$Comp
L Device:Fuse_Small F1
U 1 1 5FA167B5
P 3200 1050
F 0 "F1" H 3200 1235 50  0000 C CNN
F 1 "5A" H 3200 1144 50  0000 C CNN
F 2 "Fuse:Fuse_BelFuse_0ZRE0012FF_L8.3mm_W3.8mm" H 3200 1050 50  0001 C CNN
F 3 "~" H 3200 1050 50  0001 C CNN
	1    3200 1050
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Schottky_Small D1
U 1 1 5FA184E4
P 2900 1050
F 0 "D1" H 2900 845 50  0000 C CNN
F 1 "???" H 2900 936 50  0000 C CNN
F 2 "Diode_THT:D_DO-15_P2.54mm_Vertical_AnodeUp" V 2900 1050 50  0001 C CNN
F 3 "~" V 2900 1050 50  0001 C CNN
	1    2900 1050
	-1   0    0    1   
$EndComp
$Comp
L Device:CP_Small C1
U 1 1 5FA1AFBD
P 3850 1250
F 0 "C1" H 3938 1296 50  0000 L CNN
F 1 "10uF" H 3938 1205 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D4.0mm_P2.00mm" H 3850 1250 50  0001 C CNN
F 3 "~" H 3850 1250 50  0001 C CNN
	1    3850 1250
	1    0    0    -1  
$EndComp
$Comp
L Device:CP_Small C2
U 1 1 5FA1BD0B
P 4600 1250
F 0 "C2" H 4688 1296 50  0000 L CNN
F 1 "10uF" H 4688 1205 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D4.0mm_P2.00mm" H 4600 1250 50  0001 C CNN
F 3 "~" H 4600 1250 50  0001 C CNN
	1    4600 1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	2700 900  2700 1050
Wire Wire Line
	2700 1050 2800 1050
Wire Wire Line
	3000 1050 3100 1050
Wire Wire Line
	3300 1050 3500 1050
Wire Wire Line
	3700 1050 3850 1050
Wire Wire Line
	3850 1150 3850 1050
Wire Wire Line
	4250 1350 4250 1450
Wire Wire Line
	3850 1350 3850 1450
Wire Wire Line
	3850 1450 4250 1450
Connection ~ 4250 1450
Wire Wire Line
	4250 1450 4250 1500
Wire Wire Line
	4600 950  4600 1050
Wire Wire Line
	4550 1050 4600 1050
Connection ~ 4600 1050
Wire Wire Line
	4600 1050 4600 1150
Wire Wire Line
	4250 1450 4600 1450
Wire Wire Line
	4600 1450 4600 1350
NoConn ~ 2500 3500
NoConn ~ 2500 3600
NoConn ~ 2500 3700
NoConn ~ 2500 3800
NoConn ~ 2500 3900
NoConn ~ 2500 4200
NoConn ~ 2500 4800
NoConn ~ 2500 4900
NoConn ~ 1300 4700
NoConn ~ 1300 4800
NoConn ~ 1300 4900
NoConn ~ 2500 5200
NoConn ~ 2500 5300
NoConn ~ 2500 5400
NoConn ~ 2500 5600
NoConn ~ 1300 5000
NoConn ~ 1300 5100
NoConn ~ 1300 5200
Wire Wire Line
	3950 1050 3850 1050
Connection ~ 3850 1050
NoConn ~ 2500 5000
$Comp
L power:+3.3V #PWR0118
U 1 1 5FA643DD
P 5350 1900
F 0 "#PWR0118" H 5350 1750 50  0001 C CNN
F 1 "+3.3V" H 5365 2073 50  0000 C CNN
F 2 "" H 5350 1900 50  0001 C CNN
F 3 "" H 5350 1900 50  0001 C CNN
	1    5350 1900
	1    0    0    -1  
$EndComp
Text Notes 3200 4300 0    50   ~ 0
Add some indicator\nLEDs
Text Notes 2900 700  0    50   ~ 0
This may not be needed given\nthat we have the buck converter.
Text Notes 2500 2950 0    50   ~ 0
For the mošnjiček board,\nwill probably need some circuit\nto allow us to drive the motor \nusing 3V3 logic.
Text GLabel 2500 4400 2    50   Input ~ 0
MOTOR_ENABLE
$Comp
L Switch:SW_SPDT SW?
U 1 1 5FA79A48
P 6550 3600
F 0 "SW?" H 6350 3850 50  0000 C CNN
F 1 "SW_SPDT" H 6350 3750 50  0000 C CNN
F 2 "" H 6550 3600 50  0001 C CNN
F 3 "~" H 6550 3600 50  0001 C CNN
	1    6550 3600
	1    0    0    -1  
$EndComp
Text GLabel 6350 3600 0    50   Input ~ 0
MOTOR_ENABLE
$Comp
L power:+3.3V #PWR?
U 1 1 5FA7A359
P 6750 3400
F 0 "#PWR?" H 6750 3250 50  0001 C CNN
F 1 "+3.3V" H 6765 3573 50  0000 C CNN
F 2 "" H 6750 3400 50  0001 C CNN
F 3 "" H 6750 3400 50  0001 C CNN
	1    6750 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6750 3400 6750 3500
$Comp
L power:GND #PWR?
U 1 1 5FA7AFB4
P 6750 3800
F 0 "#PWR?" H 6750 3550 50  0001 C CNN
F 1 "GND" H 6755 3627 50  0000 C CNN
F 2 "" H 6750 3800 50  0001 C CNN
F 3 "" H 6750 3800 50  0001 C CNN
	1    6750 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	6750 3700 6750 3800
$Comp
L Connector_Generic:Conn_01x03 J?
U 1 1 5FA8085B
P 6650 2650
F 0 "J?" H 6730 2692 50  0000 L CNN
F 1 "SPEED_POT" H 6730 2601 50  0000 L CNN
F 2 "" H 6650 2650 50  0001 C CNN
F 3 "~" H 6650 2650 50  0001 C CNN
	1    6650 2650
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5FA813C3
P 6350 2450
F 0 "#PWR?" H 6350 2300 50  0001 C CNN
F 1 "+3.3V" H 6365 2623 50  0000 C CNN
F 2 "" H 6350 2450 50  0001 C CNN
F 3 "" H 6350 2450 50  0001 C CNN
	1    6350 2450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FA82538
P 6350 2850
F 0 "#PWR?" H 6350 2600 50  0001 C CNN
F 1 "GND" H 6355 2677 50  0000 C CNN
F 2 "" H 6350 2850 50  0001 C CNN
F 3 "" H 6350 2850 50  0001 C CNN
	1    6350 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	6450 2750 6350 2750
Wire Wire Line
	6350 2750 6350 2850
Wire Wire Line
	6350 2450 6350 2550
Wire Wire Line
	6350 2550 6450 2550
Text GLabel 6450 2650 0    50   Input ~ 0
SPEED_POT
$EndSCHEMATC
