# Clinostat Control Board

Schematics and PCB layout for the clinostat control board. This is part of the [Open Source Clinostat](https://tranxxenolab.net/projects/clinostat/).

Licensed under the GPLv3.


# Support

Thanks to Lovrenc Košenina for his extensive design, fabrication, and electronics assistance.

Created during a [Biofriction](https://biofriction.org/) residency.

Produced by [Galerija Kapelica](http://kapelica.org/)/[Zavod Kersnikova](https://kersnikova.org/) in Ljubljana, Slovenia.

Biofriction is supported by the European Commission--Creative Europe.
