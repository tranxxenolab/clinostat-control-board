EESchema Schematic File Version 4
LIBS:clinostat_esp32-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Schematic for Clinostat and Saccular Fount"
Date "2021.5.29"
Rev "v0.1.4"
Comp "Adriana Knouf"
Comment1 "Kersnikova Institute"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Personal_Library:XL4015_BOARD U2
U 1 1 5F99E8ED
P 1300 1000
F 0 "U2" H 1300 1225 50  0000 C CNN
F 1 "XL4015_BOARD" H 1300 1134 50  0000 C CNN
F 2 "Personal_Footprint_Library:XL4015_BOARD" H 1300 1000 50  0001 C CNN
F 3 "" H 1300 1000 50  0001 C CNN
	1    1300 1000
	1    0    0    -1  
$EndComp
$Comp
L power:+24V #PWR0103
U 1 1 5F99FA37
P 850 900
F 0 "#PWR0103" H 850 750 50  0001 C CNN
F 1 "+24V" H 865 1073 50  0000 C CNN
F 2 "" H 850 900 50  0001 C CNN
F 3 "" H 850 900 50  0001 C CNN
	1    850  900 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 5F99FFA6
P 850 1200
F 0 "#PWR0104" H 850 950 50  0001 C CNN
F 1 "GND" H 855 1027 50  0000 C CNN
F 2 "" H 850 1200 50  0001 C CNN
F 3 "" H 850 1200 50  0001 C CNN
	1    850  1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	850  900  850  1000
Wire Wire Line
	850  1000 950  1000
Wire Wire Line
	850  1200 850  1100
Wire Wire Line
	850  1100 950  1100
$Comp
L power:+5V #PWR0105
U 1 1 5F9A096E
P 1750 900
F 0 "#PWR0105" H 1750 750 50  0001 C CNN
F 1 "+5V" H 1765 1073 50  0000 C CNN
F 2 "" H 1750 900 50  0001 C CNN
F 3 "" H 1750 900 50  0001 C CNN
	1    1750 900 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0106
U 1 1 5F9A1470
P 1750 1200
F 0 "#PWR0106" H 1750 950 50  0001 C CNN
F 1 "GND" H 1755 1027 50  0000 C CNN
F 2 "" H 1750 1200 50  0001 C CNN
F 3 "" H 1750 1200 50  0001 C CNN
	1    1750 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	1650 1000 1750 1000
Wire Wire Line
	1750 1000 1750 900 
Wire Wire Line
	1650 1100 1750 1100
Wire Wire Line
	1750 1100 1750 1200
$Comp
L dk_PMIC-Voltage-Regulators-Linear:LD1117V33 U3
U 1 1 5F9A28D3
P 2500 1050
F 0 "U3" H 2500 1337 60  0000 C CNN
F 1 "LD1117V33" H 2500 1231 60  0000 C CNN
F 2 "digikey-footprints:TO-220-3" H 2700 1250 60  0001 L CNN
F 3 "http://www.st.com/content/ccc/resource/technical/document/datasheet/99/3b/7d/91/91/51/4b/be/CD00000544.pdf/files/CD00000544.pdf/jcr:content/translations/en.CD00000544.pdf" H 2700 1350 60  0001 L CNN
F 4 "497-1491-5-ND" H 2700 1450 60  0001 L CNN "Digi-Key_PN"
F 5 "LD1117V33" H 2700 1550 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 2700 1650 60  0001 L CNN "Category"
F 7 "PMIC - Voltage Regulators - Linear" H 2700 1750 60  0001 L CNN "Family"
F 8 "http://www.st.com/content/ccc/resource/technical/document/datasheet/99/3b/7d/91/91/51/4b/be/CD00000544.pdf/files/CD00000544.pdf/jcr:content/translations/en.CD00000544.pdf" H 2700 1850 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/stmicroelectronics/LD1117V33/497-1491-5-ND/586012" H 2700 1950 60  0001 L CNN "DK_Detail_Page"
F 10 "IC REG LINEAR 3.3V 800MA TO220AB" H 2700 2050 60  0001 L CNN "Description"
F 11 "STMicroelectronics" H 2700 2150 60  0001 L CNN "Manufacturer"
F 12 "Active" H 2700 2250 60  0001 L CNN "Status"
	1    2500 1050
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0107
U 1 1 5F9A3E98
P 2100 900
F 0 "#PWR0107" H 2100 750 50  0001 C CNN
F 1 "+5V" H 2115 1073 50  0000 C CNN
F 2 "" H 2100 900 50  0001 C CNN
F 3 "" H 2100 900 50  0001 C CNN
	1    2100 900 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0108
U 1 1 5F9A48A2
P 2500 1500
F 0 "#PWR0108" H 2500 1250 50  0001 C CNN
F 1 "GND" H 2505 1327 50  0000 C CNN
F 2 "" H 2500 1500 50  0001 C CNN
F 3 "" H 2500 1500 50  0001 C CNN
	1    2500 1500
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0109
U 1 1 5F9A50E7
P 2850 950
F 0 "#PWR0109" H 2850 800 50  0001 C CNN
F 1 "+3.3V" H 2865 1123 50  0000 C CNN
F 2 "" H 2850 950 50  0001 C CNN
F 3 "" H 2850 950 50  0001 C CNN
	1    2850 950 
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0110
U 1 1 5F9A6BB6
P 1650 4200
F 0 "#PWR0110" H 1650 4050 50  0001 C CNN
F 1 "+5V" H 1665 4373 50  0000 C CNN
F 2 "" H 1650 4200 50  0001 C CNN
F 3 "" H 1650 4200 50  0001 C CNN
	1    1650 4200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0111
U 1 1 5F9A78CA
P 1650 7000
F 0 "#PWR0111" H 1650 6750 50  0001 C CNN
F 1 "GND" H 1655 6827 50  0000 C CNN
F 2 "" H 1650 7000 50  0001 C CNN
F 3 "" H 1650 7000 50  0001 C CNN
	1    1650 7000
	1    0    0    -1  
$EndComp
$Comp
L RF_Module:ESP32-WROOM-32D U1
U 1 1 5F9C381E
P 1650 5600
F 0 "U1" H 2250 7150 50  0000 C CNN
F 1 "ESP32-WROOM-32D" H 2500 7000 50  0000 C CNN
F 2 "Personal_Footprint_Library:AZ-Delivery_ESP32" H 1650 4100 50  0001 C CNN
F 3 "https://www.espressif.com/sites/default/files/documentation/esp32-wroom-32d_esp32-wroom-32u_datasheet_en.pdf" H 1350 5650 50  0001 C CNN
	1    1650 5600
	1    0    0    -1  
$EndComp
NoConn ~ 1050 4400
NoConn ~ 1050 4600
NoConn ~ 1050 4700
$Comp
L Connector_Generic:Conn_01x02 J2
U 1 1 5F9F2723
P 4850 950
F 0 "J2" H 4930 942 50  0000 L CNN
F 1 "5V_RAIL" H 4930 851 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 4850 950 50  0001 C CNN
F 3 "~" H 4850 950 50  0001 C CNN
	1    4850 950 
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0101
U 1 1 5F9F3616
P 4550 900
F 0 "#PWR0101" H 4550 750 50  0001 C CNN
F 1 "+5V" H 4565 1073 50  0000 C CNN
F 2 "" H 4550 900 50  0001 C CNN
F 3 "" H 4550 900 50  0001 C CNN
	1    4550 900 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0113
U 1 1 5F9F3C92
P 4550 1150
F 0 "#PWR0113" H 4550 900 50  0001 C CNN
F 1 "GND" H 4555 977 50  0000 C CNN
F 2 "" H 4550 1150 50  0001 C CNN
F 3 "" H 4550 1150 50  0001 C CNN
	1    4550 1150
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J4
U 1 1 5F9F4F28
P 5850 950
F 0 "J4" H 5930 942 50  0000 L CNN
F 1 "3V3_RAIL" H 5930 851 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 5850 950 50  0001 C CNN
F 3 "~" H 5850 950 50  0001 C CNN
	1    5850 950 
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0114
U 1 1 5F9F553E
P 5550 900
F 0 "#PWR0114" H 5550 750 50  0001 C CNN
F 1 "+3.3V" H 5565 1073 50  0000 C CNN
F 2 "" H 5550 900 50  0001 C CNN
F 3 "" H 5550 900 50  0001 C CNN
	1    5550 900 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0115
U 1 1 5F9F5F35
P 5550 1150
F 0 "#PWR0115" H 5550 900 50  0001 C CNN
F 1 "GND" H 5555 977 50  0000 C CNN
F 2 "" H 5550 1150 50  0001 C CNN
F 3 "" H 5550 1150 50  0001 C CNN
	1    5550 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 950  5550 950 
Wire Wire Line
	5550 950  5550 900 
Wire Wire Line
	4550 900  4550 950 
Wire Wire Line
	4550 950  4650 950 
Wire Wire Line
	4650 1050 4550 1050
Wire Wire Line
	4550 1050 4550 1150
Wire Wire Line
	5550 1150 5550 1050
Wire Wire Line
	5550 1050 5650 1050
Text GLabel 2250 5500 2    50   Input ~ 0
DIR
Text GLabel 2250 5400 2    50   Input ~ 0
STEP
Text GLabel 2250 5900 2    50   Input ~ 0
SCL
Text GLabel 2250 5800 2    50   Input ~ 0
SDA
Text GLabel 2250 6600 2    50   Input ~ 0
SPEED_POT
Text GLabel 2250 6700 2    50   Input ~ 0
SPI_RESET
Text GLabel 2250 6400 2    50   Input ~ 0
SPI_DC
Text GLabel 2250 6000 2    50   Input ~ 0
SPI_MOSI
Text GLabel 2250 5600 2    50   Input ~ 0
SPI_SCK
Text GLabel 2250 4900 2    50   Input ~ 0
SPI_SS
$Comp
L Connector_Generic:Conn_01x04 J3
U 1 1 5FA01373
P 4850 1900
F 0 "J3" H 4930 1892 50  0000 L CNN
F 1 "ACCEL_I2C" H 4930 1801 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 4850 1900 50  0001 C CNN
F 3 "~" H 4850 1900 50  0001 C CNN
	1    4850 1900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0119
U 1 1 5FA0308C
P 4400 2250
F 0 "#PWR0119" H 4400 2000 50  0001 C CNN
F 1 "GND" H 4405 2077 50  0000 C CNN
F 2 "" H 4400 2250 50  0001 C CNN
F 3 "" H 4400 2250 50  0001 C CNN
	1    4400 2250
	1    0    0    -1  
$EndComp
Text GLabel 4650 2000 0    50   Input ~ 0
SDA
Text GLabel 4650 2100 0    50   Input ~ 0
SCL
Wire Wire Line
	4400 2250 4400 1900
Wire Wire Line
	4400 1900 4650 1900
Wire Wire Line
	4650 1800 4550 1800
Wire Wire Line
	4550 1800 4550 1700
$Comp
L Connector_Generic:Conn_01x02 J5
U 1 1 5FA072AB
P 5850 1700
F 0 "J5" H 5930 1692 50  0000 L CNN
F 1 "MOTOR_CTL" H 5930 1601 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 5850 1700 50  0001 C CNN
F 3 "~" H 5850 1700 50  0001 C CNN
	1    5850 1700
	1    0    0    -1  
$EndComp
Text GLabel 5650 1700 0    50   Input ~ 0
DIR
Text GLabel 5650 1800 0    50   Input ~ 0
STEP
Text Notes 4800 1700 0    50   ~ 0
I2C
Text Notes 5800 1600 0    50   ~ 0
Motor
$Comp
L Connector_Generic:Conn_01x08 J6
U 1 1 5FA09003
P 7200 2100
F 0 "J6" H 7280 2092 50  0000 L CNN
F 1 "SCREEN_SPI" H 7280 2001 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x08_P2.54mm_Vertical" H 7200 2100 50  0001 C CNN
F 3 "~" H 7200 2100 50  0001 C CNN
	1    7200 2100
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0120
U 1 1 5FA09BDD
P 6900 1700
F 0 "#PWR0120" H 6900 1550 50  0001 C CNN
F 1 "+3.3V" H 6915 1873 50  0000 C CNN
F 2 "" H 6900 1700 50  0001 C CNN
F 3 "" H 6900 1700 50  0001 C CNN
	1    6900 1700
	1    0    0    -1  
$EndComp
Text GLabel 7000 1900 0    50   Input ~ 0
SPI_RESET
Text GLabel 7000 2000 0    50   Input ~ 0
SPI_DC
Text GLabel 7000 2100 0    50   Input ~ 0
SPI_SS
Text GLabel 7000 2200 0    50   Input ~ 0
SPI_MOSI
Text GLabel 7000 2300 0    50   Input ~ 0
SPI_SCK
$Comp
L power:GND #PWR0121
U 1 1 5FA0BE1C
P 6900 2550
F 0 "#PWR0121" H 6900 2300 50  0001 C CNN
F 1 "GND" H 6905 2377 50  0000 C CNN
F 2 "" H 6900 2550 50  0001 C CNN
F 3 "" H 6900 2550 50  0001 C CNN
	1    6900 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	7000 2500 6900 2500
Wire Wire Line
	6900 2500 6900 2550
Wire Wire Line
	6900 1700 6900 1800
Wire Wire Line
	6900 1800 7000 1800
Wire Wire Line
	7000 2400 6550 2400
Wire Wire Line
	6550 2400 6550 1800
Wire Wire Line
	6550 1800 6900 1800
Connection ~ 6900 1800
$Comp
L Mechanical:MountingHole_Pad H1
U 1 1 5FA12293
P 4700 5450
F 0 "H1" V 4654 5600 50  0000 L CNN
F 1 "MountingHole_Pad" V 4745 5600 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 4700 5450 50  0001 C CNN
F 3 "~" H 4700 5450 50  0001 C CNN
	1    4700 5450
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H2
U 1 1 5FA12FBF
P 4700 5650
F 0 "H2" V 4654 5800 50  0000 L CNN
F 1 "MountingHole_Pad" V 4745 5800 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 4700 5650 50  0001 C CNN
F 3 "~" H 4700 5650 50  0001 C CNN
	1    4700 5650
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H3
U 1 1 5FA132B6
P 4700 5850
F 0 "H3" V 4654 6000 50  0000 L CNN
F 1 "MountingHole_Pad" V 4745 6000 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 4700 5850 50  0001 C CNN
F 3 "~" H 4700 5850 50  0001 C CNN
	1    4700 5850
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H4
U 1 1 5FA135C7
P 4700 6050
F 0 "H4" V 4654 6200 50  0000 L CNN
F 1 "MountingHole_Pad" V 4745 6200 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 4700 6050 50  0001 C CNN
F 3 "~" H 4700 6050 50  0001 C CNN
	1    4700 6050
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0122
U 1 1 5FA13B8A
P 4500 6200
F 0 "#PWR0122" H 4500 5950 50  0001 C CNN
F 1 "GND" H 4505 6027 50  0000 C CNN
F 2 "" H 4500 6200 50  0001 C CNN
F 3 "" H 4500 6200 50  0001 C CNN
	1    4500 6200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 6200 4500 6050
Wire Wire Line
	4500 5450 4600 5450
Wire Wire Line
	4600 5650 4500 5650
Connection ~ 4500 5650
Wire Wire Line
	4500 5650 4500 5450
Wire Wire Line
	4600 5850 4500 5850
Connection ~ 4500 5850
Wire Wire Line
	4500 5850 4500 5650
Wire Wire Line
	4600 6050 4500 6050
Connection ~ 4500 6050
Wire Wire Line
	4500 6050 4500 5850
$Comp
L Device:CP_Small C1
U 1 1 5FA1AFBD
P 2100 1250
F 0 "C1" H 2188 1296 50  0000 L CNN
F 1 "10uF" H 2188 1205 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D4.0mm_P2.00mm" H 2100 1250 50  0001 C CNN
F 3 "~" H 2100 1250 50  0001 C CNN
	1    2100 1250
	1    0    0    -1  
$EndComp
$Comp
L Device:CP_Small C2
U 1 1 5FA1BD0B
P 2850 1250
F 0 "C2" H 2938 1296 50  0000 L CNN
F 1 "10uF" H 2938 1205 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D4.0mm_P2.00mm" H 2850 1250 50  0001 C CNN
F 3 "~" H 2850 1250 50  0001 C CNN
	1    2850 1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	2100 900  2100 1050
Wire Wire Line
	2100 1150 2100 1050
Wire Wire Line
	2500 1350 2500 1450
Wire Wire Line
	2100 1350 2100 1450
Wire Wire Line
	2100 1450 2500 1450
Connection ~ 2500 1450
Wire Wire Line
	2500 1450 2500 1500
Wire Wire Line
	2850 950  2850 1050
Wire Wire Line
	2800 1050 2850 1050
Connection ~ 2850 1050
Wire Wire Line
	2850 1050 2850 1150
Wire Wire Line
	2500 1450 2850 1450
Wire Wire Line
	2850 1450 2850 1350
NoConn ~ 2250 4400
NoConn ~ 2250 4500
NoConn ~ 2250 4600
NoConn ~ 2250 4700
NoConn ~ 2250 5100
NoConn ~ 1050 5600
NoConn ~ 1050 5700
NoConn ~ 1050 5800
NoConn ~ 1050 5900
NoConn ~ 1050 6000
NoConn ~ 1050 6100
Wire Wire Line
	2200 1050 2100 1050
$Comp
L power:+3.3V #PWR0118
U 1 1 5FA643DD
P 4550 1700
F 0 "#PWR0118" H 4550 1550 50  0001 C CNN
F 1 "+3.3V" H 4565 1873 50  0000 C CNN
F 2 "" H 4550 1700 50  0001 C CNN
F 3 "" H 4550 1700 50  0001 C CNN
	1    4550 1700
	1    0    0    -1  
$EndComp
Text Notes 8500 1000 0    50   ~ 0
For the mošnjiček board,\nwill probably need some circuit\nto allow us to drive the pump\nmotor using 3V3 logic.
Text GLabel 2250 5300 2    50   Input ~ 0
MOTOR_ENABLE
Text GLabel 7050 1050 0    50   Input ~ 0
MOTOR_ENABLE
$Comp
L Connector_Generic:Conn_01x03 J1
U 1 1 5FA8085B
P 5850 2450
F 0 "J1" H 5930 2492 50  0000 L CNN
F 1 "SPEED_POT" H 5930 2401 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 5850 2450 50  0001 C CNN
F 3 "~" H 5850 2450 50  0001 C CNN
	1    5850 2450
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR02
U 1 1 5FA813C3
P 5550 2250
F 0 "#PWR02" H 5550 2100 50  0001 C CNN
F 1 "+3.3V" H 5565 2423 50  0000 C CNN
F 2 "" H 5550 2250 50  0001 C CNN
F 3 "" H 5550 2250 50  0001 C CNN
	1    5550 2250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR03
U 1 1 5FA82538
P 5550 2650
F 0 "#PWR03" H 5550 2400 50  0001 C CNN
F 1 "GND" H 5555 2477 50  0000 C CNN
F 2 "" H 5550 2650 50  0001 C CNN
F 3 "" H 5550 2650 50  0001 C CNN
	1    5550 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 2550 5550 2550
Wire Wire Line
	5550 2550 5550 2650
Wire Wire Line
	5550 2250 5550 2350
Wire Wire Line
	5550 2350 5650 2350
Text GLabel 5650 2450 0    50   Input ~ 0
SPEED_POT
Connection ~ 2100 1050
Text GLabel 2250 4800 2    50   Input ~ 0
POWER_LED
Text GLabel 2250 6300 2    50   Input ~ 0
ERROR_LED
Wire Wire Line
	5550 3400 5550 3500
Wire Wire Line
	5550 3050 5550 3200
$Comp
L power:GND #PWR04
U 1 1 5FAB5B7A
P 5550 3900
F 0 "#PWR04" H 5550 3650 50  0001 C CNN
F 1 "GND" H 5555 3727 50  0000 C CNN
F 2 "" H 5550 3900 50  0001 C CNN
F 3 "" H 5550 3900 50  0001 C CNN
	1    5550 3900
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R2
U 1 1 5FAB5577
P 5550 3300
F 0 "R2" H 5609 3346 50  0000 L CNN
F 1 "100" H 5609 3255 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 5550 3300 50  0001 C CNN
F 3 "~" H 5550 3300 50  0001 C CNN
	1    5550 3300
	1    0    0    -1  
$EndComp
Text GLabel 5550 3050 0    50   Input ~ 0
ERROR_LED
$Comp
L Device:R_Small R1
U 1 1 5FAB0C34
P 4850 3300
F 0 "R1" H 4909 3346 50  0000 L CNN
F 1 "100" H 4909 3255 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 4850 3300 50  0001 C CNN
F 3 "~" H 4850 3300 50  0001 C CNN
	1    4850 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4850 3400 4850 3500
Wire Wire Line
	4850 3050 4850 3200
$Comp
L power:GND #PWR01
U 1 1 5FAB1172
P 4850 3900
F 0 "#PWR01" H 4850 3650 50  0001 C CNN
F 1 "GND" H 4855 3727 50  0000 C CNN
F 2 "" H 4850 3900 50  0001 C CNN
F 3 "" H 4850 3900 50  0001 C CNN
	1    4850 3900
	1    0    0    -1  
$EndComp
Text GLabel 4850 3050 0    50   Input ~ 0
POWER_LED
Wire Notes Line
	650  650  650  1750
Wire Notes Line
	650  1750 3200 1750
Wire Notes Line
	3200 1750 3200 650 
Wire Notes Line
	3200 650  650  650 
Text Notes 650  1850 0    50   ~ 0
Input Power
Text Notes 4300 5100 0    50   ~ 0
Peripherals, Outputs, and Switches
Wire Notes Line
	4300 5300 4300 6500
Wire Notes Line
	4300 6500 5650 6500
Wire Notes Line
	5650 6500 5650 5300
Wire Notes Line
	5650 5300 4300 5300
Text Notes 4300 6600 0    50   ~ 0
Mounting
Wire Notes Line
	800  3900 800  7250
Wire Notes Line
	800  7250 3100 7250
Wire Notes Line
	3100 7250 3100 3900
Wire Notes Line
	3100 3900 800  3900
Text Notes 800  7400 0    50   ~ 0
ESP32 and Connections
Wire Notes Line
	8400 600  8400 3800
Wire Notes Line
	8400 3800 11050 3800
Wire Notes Line
	11050 3800 11050 600 
Wire Notes Line
	11050 600  8400 600 
Text Notes 8400 3950 0    50   ~ 0
Notes
$Comp
L power:GND #PWR05
U 1 1 5FAD6C8F
P 6250 3450
F 0 "#PWR05" H 6250 3200 50  0001 C CNN
F 1 "GND" H 6255 3277 50  0000 C CNN
F 2 "" H 6250 3450 50  0001 C CNN
F 3 "" H 6250 3450 50  0001 C CNN
	1    6250 3450
	1    0    0    -1  
$EndComp
Text GLabel 6300 3050 0    50   Input ~ 0
SDA
Text GLabel 6300 3150 0    50   Input ~ 0
SCL
Text Notes 6450 2950 0    50   ~ 0
I2C
$Comp
L power:+3.3V #PWR06
U 1 1 5FAD6C9C
P 6000 2950
F 0 "#PWR06" H 6000 2800 50  0001 C CNN
F 1 "+3.3V" H 6015 3123 50  0000 C CNN
F 2 "" H 6000 2950 50  0001 C CNN
F 3 "" H 6000 2950 50  0001 C CNN
	1    6000 2950
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J8
U 1 1 5FAD90E7
P 7550 3150
F 0 "J8" H 7630 3142 50  0000 L CNN
F 1 "MISC1_I2C" H 7630 3051 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 7550 3150 50  0001 C CNN
F 3 "~" H 7550 3150 50  0001 C CNN
	1    7550 3150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR07
U 1 1 5FAD90ED
P 7100 3500
F 0 "#PWR07" H 7100 3250 50  0001 C CNN
F 1 "GND" H 7105 3327 50  0000 C CNN
F 2 "" H 7100 3500 50  0001 C CNN
F 3 "" H 7100 3500 50  0001 C CNN
	1    7100 3500
	1    0    0    -1  
$EndComp
Text GLabel 7350 3250 0    50   Input ~ 0
SDA
Text GLabel 7350 3350 0    50   Input ~ 0
SCL
Wire Wire Line
	7100 3500 7100 3150
Wire Wire Line
	7100 3150 7350 3150
Wire Wire Line
	7350 3050 7250 3050
Wire Wire Line
	7250 3050 7250 2950
Text Notes 7500 2950 0    50   ~ 0
I2C
$Comp
L power:+3.3V #PWR08
U 1 1 5FAD90FA
P 7250 2950
F 0 "#PWR08" H 7250 2800 50  0001 C CNN
F 1 "+3.3V" H 7265 3123 50  0000 C CNN
F 2 "" H 7250 2950 50  0001 C CNN
F 3 "" H 7250 2950 50  0001 C CNN
	1    7250 2950
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J7
U 1 1 5FAD6C89
P 6500 3150
F 0 "J7" H 6580 3142 50  0000 L CNN
F 1 "OLED" H 6580 3051 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 6500 3150 50  0001 C CNN
F 3 "~" H 6500 3150 50  0001 C CNN
	1    6500 3150
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J9
U 1 1 5FAE787F
P 7250 1050
F 0 "J9" H 7330 1092 50  0000 L CNN
F 1 "MOTOR_EN_SPDT" H 7330 1001 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 7250 1050 50  0001 C CNN
F 3 "~" H 7250 1050 50  0001 C CNN
	1    7250 1050
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR09
U 1 1 5FAE8649
P 7050 850
F 0 "#PWR09" H 7050 700 50  0001 C CNN
F 1 "+3.3V" H 7065 1023 50  0000 C CNN
F 2 "" H 7050 850 50  0001 C CNN
F 3 "" H 7050 850 50  0001 C CNN
	1    7050 850 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR010
U 1 1 5FAE8F2F
P 7050 1250
F 0 "#PWR010" H 7050 1000 50  0001 C CNN
F 1 "GND" H 7055 1077 50  0000 C CNN
F 2 "" H 7050 1250 50  0001 C CNN
F 3 "" H 7050 1250 50  0001 C CNN
	1    7050 1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	7050 850  7050 950 
Wire Wire Line
	7050 1150 7050 1250
$Comp
L Device:LED D1
U 1 1 5FAFD183
P 4850 3650
F 0 "D1" V 4889 3532 50  0000 R CNN
F 1 "POWER" V 4798 3532 50  0000 R CNN
F 2 "LED_THT:LED_D3.0mm" H 4850 3650 50  0001 C CNN
F 3 "~" H 4850 3650 50  0001 C CNN
	1    4850 3650
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D2
U 1 1 5FAFE7FC
P 5550 3650
F 0 "D2" V 5589 3532 50  0000 R CNN
F 1 "ERROR" V 5498 3532 50  0000 R CNN
F 2 "LED_THT:LED_D3.0mm" H 5550 3650 50  0001 C CNN
F 3 "~" H 5550 3650 50  0001 C CNN
	1    5550 3650
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5550 3800 5550 3900
Wire Wire Line
	4850 3800 4850 3900
Wire Notes Line
	8100 600  4300 600 
NoConn ~ 2250 5000
NoConn ~ 2250 5200
NoConn ~ 2250 5700
NoConn ~ 2250 6200
NoConn ~ 2250 6500
Text GLabel 2250 6100 2    50   Input ~ 0
PUMP_PWM
Text GLabel 6100 4500 0    50   Input ~ 0
PUMP_PWM
$Comp
L power:+3V3 #PWR0102
U 1 1 5FB30B33
P 6300 4100
F 0 "#PWR0102" H 6300 3950 50  0001 C CNN
F 1 "+3V3" H 6315 4273 50  0000 C CNN
F 2 "" H 6300 4100 50  0001 C CNN
F 3 "" H 6300 4100 50  0001 C CNN
	1    6300 4100
	1    0    0    -1  
$EndComp
$Comp
L dk_Transistors-FETs-MOSFETs-Single:2N7000 Q1
U 1 1 5FB32890
P 6650 4500
F 0 "Q1" V 6811 4500 60  0000 C CNN
F 1 "2N7000" V 6917 4500 60  0000 C CNN
F 2 "digikey-footprints:TO-92-3" H 6850 4700 60  0001 L CNN
F 3 "https://www.onsemi.com/pub/Collateral/NDS7002A-D.PDF" H 6850 4800 60  0001 L CNN
F 4 "2N7000FS-ND" H 6850 4900 60  0001 L CNN "Digi-Key_PN"
F 5 "2N7000" H 6850 5000 60  0001 L CNN "MPN"
F 6 "Discrete Semiconductor Products" H 6850 5100 60  0001 L CNN "Category"
F 7 "Transistors - FETs, MOSFETs - Single" H 6850 5200 60  0001 L CNN "Family"
F 8 "https://www.onsemi.com/pub/Collateral/NDS7002A-D.PDF" H 6850 5300 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/on-semiconductor/2N7000/2N7000FS-ND/244278" H 6850 5400 60  0001 L CNN "DK_Detail_Page"
F 10 "MOSFET N-CH 60V 200MA TO-92" H 6850 5500 60  0001 L CNN "Description"
F 11 "ON Semiconductor" H 6850 5600 60  0001 L CNN "Manufacturer"
F 12 "Active" H 6850 5700 60  0001 L CNN "Status"
	1    6650 4500
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R3
U 1 1 5FB351B0
P 6300 4300
F 0 "R3" H 6150 4350 50  0000 L CNN
F 1 "10K" H 6100 4250 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 6300 4300 50  0001 C CNN
F 3 "~" H 6300 4300 50  0001 C CNN
	1    6300 4300
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0112
U 1 1 5FB36824
P 7000 4100
F 0 "#PWR0112" H 7000 3950 50  0001 C CNN
F 1 "+5V" H 7015 4273 50  0000 C CNN
F 2 "" H 7000 4100 50  0001 C CNN
F 3 "" H 7000 4100 50  0001 C CNN
	1    7000 4100
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R4
U 1 1 5FB3767F
P 7000 4300
F 0 "R4" H 7059 4346 50  0000 L CNN
F 1 "10K" H 7059 4255 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 7000 4300 50  0001 C CNN
F 3 "~" H 7000 4300 50  0001 C CNN
	1    7000 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	6300 4100 6300 4150
Wire Wire Line
	6100 4500 6300 4500
Wire Wire Line
	6300 4400 6300 4500
Connection ~ 6300 4500
Wire Wire Line
	6300 4500 6450 4500
Wire Wire Line
	6550 4200 6550 4150
Wire Wire Line
	6550 4150 6300 4150
Connection ~ 6300 4150
Wire Wire Line
	6300 4150 6300 4200
Wire Wire Line
	7000 4100 7000 4200
Wire Wire Line
	7000 4400 7000 4500
Wire Wire Line
	7000 4500 6850 4500
$Comp
L power:GND #PWR0116
U 1 1 5FB4586C
P 7350 4700
F 0 "#PWR0116" H 7350 4450 50  0001 C CNN
F 1 "GND" H 7355 4527 50  0000 C CNN
F 2 "" H 7350 4700 50  0001 C CNN
F 3 "" H 7350 4700 50  0001 C CNN
	1    7350 4700
	1    0    0    -1  
$EndComp
Wire Notes Line
	8100 5000 4300 5000
Wire Notes Line
	4300 600  4300 5000
Wire Notes Line
	8100 600  8100 5000
Wire Wire Line
	6300 3350 6250 3350
Wire Wire Line
	6250 3350 6250 3450
Wire Wire Line
	6000 2950 6000 3250
Wire Wire Line
	6000 3250 6300 3250
$Comp
L Connector_Generic:Conn_01x03 J10
U 1 1 5FC19772
P 7550 4500
F 0 "J10" H 7630 4542 50  0000 L CNN
F 1 "PWM" H 7630 4451 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 7550 4500 50  0001 C CNN
F 3 "~" H 7550 4500 50  0001 C CNN
	1    7550 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	7350 4500 7000 4500
Connection ~ 7000 4500
Wire Wire Line
	7350 4600 7350 4700
$Comp
L power:+5V #PWR0117
U 1 1 5FC1F129
P 7350 4250
F 0 "#PWR0117" H 7350 4100 50  0001 C CNN
F 1 "+5V" H 7365 4423 50  0000 C CNN
F 2 "" H 7350 4250 50  0001 C CNN
F 3 "" H 7350 4250 50  0001 C CNN
	1    7350 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	7350 4250 7350 4400
$EndSCHEMATC
